package com.designpattern.builder;

public class EmiScheduler {

	private int emiAmount;
	private int totalNoEmi;

	public EmiScheduler(int emiAmount, int totalNoEmi) {
		super();
		this.emiAmount = emiAmount;
		this.totalNoEmi = totalNoEmi;
	}

	@Override
	public String toString() {
		return "EmiScheduler [emiAmount=" + emiAmount + ", totalNoEmi=" + totalNoEmi + "]";
	}

}
