package com.designpattern.builder;

public class AtmTransactions {

	private int withdrawalAmt;
	private int depositAmt;

	public AtmTransactions(int withdrawalAmt, int depositAmt) {
		super();
		this.withdrawalAmt = withdrawalAmt;
		this.depositAmt = depositAmt;
	}

	@Override
	public String toString() {
		return "AtmTransactions [withdrawalAmt=" + withdrawalAmt + ", depositAmt=" + depositAmt + "]";
	}
	
}
