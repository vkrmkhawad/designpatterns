package com.designpattern.builder;

public class BankAccountBuilder {

	private String bankAccNo;
	private String accountType;
	private String branch;
	private int balance;

	private AtmTransactions atmTransaction;
	private EmiScheduler emiScheduler;

	public BankAccountBuilder setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
		return this;
	}

	public BankAccountBuilder setAccountType(String accountType) {
		this.accountType = accountType;
		return this;
	}

	public BankAccountBuilder setBranch(String branch) {
		this.branch = branch;
		return this;
	}

	public BankAccountBuilder setBalance(int balance) {
		this.balance = balance;
		return this;
	}

	public BankAccountBuilder setAtmTransactions(AtmTransactions atmTransaction) {
		this.atmTransaction = atmTransaction;
		return this;
	}

	public BankAccountBuilder setEmiScheduler(EmiScheduler emiScheduler) {
		this.emiScheduler = emiScheduler;
		return this;
	}

	public BankAccount createBankAccount() {
		return new BankAccount(bankAccNo, accountType, branch, balance, atmTransaction, emiScheduler);
	}
}
