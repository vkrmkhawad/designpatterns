package com.designpattern.builder;

public class EmiScheduleBuilder {

	private int emiAmount;
	private int totalNoEmi;

	public EmiScheduleBuilder setEmiAmount(int emiAmount) {
		this.emiAmount = emiAmount;
		return this;
	}
	
	public EmiScheduleBuilder setTotalNoEmi(int totalNoEmi) {
		this.emiAmount = totalNoEmi;
		return this;
	}
	
	public EmiScheduler createEmiScheduler() {
		return new EmiScheduler(emiAmount,totalNoEmi);
	}
}
