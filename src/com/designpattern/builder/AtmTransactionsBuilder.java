package com.designpattern.builder;

public class AtmTransactionsBuilder {
	private int withdrawalAmt;
	private int depositAmt;
	
	public AtmTransactionsBuilder setWithdrawalAmt (int withdrawalAmt) {
		this.withdrawalAmt = withdrawalAmt;
		return this;
	}
	
	public AtmTransactionsBuilder setDepositAmt (int depositAmt) {
		this.depositAmt = depositAmt;
		return this;
	}
	
	public AtmTransactions createAtmTransaction() {
		return new AtmTransactions(withdrawalAmt,depositAmt);
	}
}
