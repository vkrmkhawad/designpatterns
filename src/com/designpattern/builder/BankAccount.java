package com.designpattern.builder;

public class BankAccount {

	private String bankAccNo;
	private String accountType;
	private String branch;
	private int balance;
	
	private AtmTransactions atmTransaction;
	private EmiScheduler emiScheduler;
	
	public BankAccount(String bankAccNo, String accountType, String branch, int balance, AtmTransactions atmTransaction,
			EmiScheduler emiScheduler) {
		super();
		this.bankAccNo = bankAccNo;
		this.accountType = accountType;
		this.branch = branch;
		this.balance = balance;
		this.atmTransaction = atmTransaction;
		this.emiScheduler = emiScheduler;
	}

	@Override
	public String toString() {
		return "BankAccount [bankAccNo=" + bankAccNo + ", accountType=" + accountType + ", branch=" + branch
				+ ", balance=" + balance + ", atmTransaction=" + atmTransaction + ", emiScheduler=" + emiScheduler
				+ "]";
	}
	
	
}
