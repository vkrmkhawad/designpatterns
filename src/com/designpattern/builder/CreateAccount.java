package com.designpattern.builder;

public class CreateAccount {

	public static void main(String[] args) {
		
		BankAccount bankAccount = new BankAccountBuilder().setBankAccNo("10001").setAccountType("Savings").setBranch("Blr")
				.setBalance(1000).setAtmTransactions(new AtmTransactionsBuilder().setDepositAmt(1000).createAtmTransaction()).createBankAccount();
		System.out.println("Bank account has been created : "+bankAccount.toString());
	}

}
