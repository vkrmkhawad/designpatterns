package com.designpattern.factory;

public class USDConverter implements CurrencyConverter{

	@Override
	public double convertToInr(double amount) {
		return amount*74.14;
	}

}
