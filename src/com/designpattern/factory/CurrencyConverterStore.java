package com.designpattern.factory;

public class CurrencyConverterStore {

	public static void main(String[] args) {
		CurrencyConverterFactory currencyConverterFactory = new CurrencyConverterFactory();
		System.out.println("500 GBP to INR : " + currencyConverterFactory.convertCurrency("GBP", 500));
		System.out.println("700 USD to INR : " + currencyConverterFactory.convertCurrency("USD", 700));
	}

}
