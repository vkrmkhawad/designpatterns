package com.designpattern.factory;

public class GBPConverter implements CurrencyConverter{

	@Override
	public double convertToInr(double amount) {
		return amount*101.74;
	}

}
