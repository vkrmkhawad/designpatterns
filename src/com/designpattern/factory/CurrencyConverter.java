package com.designpattern.factory;

public interface CurrencyConverter {

	double convertToInr(double amount);
}
