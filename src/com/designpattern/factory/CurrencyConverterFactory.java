package com.designpattern.factory;

public class CurrencyConverterFactory {

	private CurrencyConverter getCurrencyConverter(String currency) {
		switch(currency) {
		case "USD":
			return new USDConverter();
		case "GBP":
			return new GBPConverter();
		default:
			return null;
		}

	}
	
	public double convertCurrency (String currency, double amount) {
		CurrencyConverter currencyConverter = getCurrencyConverter(currency);
		return currencyConverter.convertToInr(amount);
	}
}
