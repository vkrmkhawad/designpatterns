package com.designpattern.singleton;

import java.sql.Connection;
import java.sql.SQLException;

public class CreateConnection {

	public static void main(String[] args) throws SQLException {

		Connection connection = DatabaseConnection.getConnection();
		if (connection != null)
			System.out.println("Connection created");
		connection.close();
	}

}
